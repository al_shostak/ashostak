package by.htp.home_work.hw3.Task1;

import by.htp.home_work.hw3.Task1.Card;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;

public class CardTest {
    private Card card;
    private String name;
    BigDecimal bd;
    BigDecimal cash;
    BigDecimal bigWithdraw;
    double index;

    @Before
    public void initCard() {
        name = new String ("S.C.Bush");
        bd = new BigDecimal("111.111");
        bigWithdraw = new BigDecimal("222.333");
        card = new Card(name, bd);
        cash = new BigDecimal("10.233");
        index = 0.1;
    }

    @After
    public void clearCard() {
        bd = null;
        card = null;
        cash = null;
        index = 0;
    }

    @Test
    public void testSetName(){
        Assert.assertEquals(card.getName(),"S.C.Bush");
    }

    @Test
    public void testSetBalance(){
        Assert.assertEquals(card.getBalance(),bd);
    }

    @Test
    public void testGetBalance() {
        Assert.assertEquals(card.getBalance(),bd);
    }

    @Test
    public void testGetName() {
        Assert.assertEquals(card.getName(), name);
    }


    @Test
    public void testDeposit(){
        card.deposit(cash);
        Assert.assertEquals(card.getBalance(),BigDecimal.valueOf(121.344));
    }

    @Test
    public void testWithdrawFromBalance() {
        card.withdrawFromBalance(cash);
        Assert.assertEquals(card.getBalance(),BigDecimal.valueOf(100.878));
    }

    @Test
    public void testIfWithdrawBiggerThenBalance(){
        card.withdrawFromBalance(bigWithdraw);
        Assert.assertEquals(card.getBalance(),null);
    }

    @Test
    public void testDisplayDifferentCurrency() {
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        card.displayDifferentCurrency(card,index);
        Assert.assertEquals(str.toString(),"Card balance in different currency = 11.1111");
    }
}