package by.htp.home_work.hw3.Task2;

import java.util.Arrays;

public final class Median {

    private Median() {};

    /**
     * Method median find "middle number" in a sorted list of numbers.
     * @param array contains a certain number of values of the type int.
     * @return value of "middle number" of type float
     */
    public static float median(int[] array) {
        float mediana = 0;

        if (array.length % 2 == 0) {
            Arrays.sort(array);
            mediana = (array[(array.length / 2) - 1] + array[array.length / 2]) / 2.0f;

        } else if (array.length % 2 == 1) {
            Arrays.sort(array);
            mediana = array[(array.length - 1) / 2];

        }
        return mediana;
    }
    /**
     * Method median find "middle number" in a sorted list of numbers.
     * @param array contains a certain number of values of the type double.
     * @return value of "middle number" of type double
     */
    public static double median(double[] array) {
        double mediana = 0;

        if (array.length % 2 == 0) {
            Arrays.sort(array);
            mediana = (array[(array.length / 2) - 1] + array[array.length / 2]) / 2;

        } else if (array.length % 2 == 1) {
            Arrays.sort(array);
            mediana = array[(array.length - 1) / 2];
        }
        return mediana;
    }
}
