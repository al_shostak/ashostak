package by.htp.home_work.hw3.Task1;

import java.math.BigDecimal;

/**
 * The {@code Card} class provides methods for deposit,
 * withdraw, convert, viewing balance status. Balance stored in {@code BigDecimal}.
 */
public class Card {
    private String name;
    private BigDecimal balance;

    /**
     * Public constructor - creation of a new card with specific values.
     * @param name {@code String} card owner's name.
     * @param balance {@code BigDecemal} the amount of money on the card.
     */
    public Card(String name, BigDecimal balance) {
        this.name = name;
        this.balance = balance;
    }

    /**
     * public constructor - creation of a new object with specific value.
     * @param name{@code String} card owner's name.
     */
    public Card(String name) {
        this.name = name;
    }

    /**
     * setBalance method allows to set values of balance.
     * @param balance (@BigDecimal) this value will be assigned as a class field value.
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return the value of balance field.
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * setName method allows to set value of user's name field.
     * @param name this value will be assigned as a class field value/
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of name field.
     */
    public String getName() {
        return name;
    }

    /**
     * deposit method adds money to the balance.
     * @param money {@code BigDecimal} amount of money that transferred to the balance.
     */
    public void deposit(BigDecimal money) {
        balance = balance.add(money);
    }

    /**
     * withdrawFromBalance method substract money from balance. If balance status less than the value of
     * "withdraw money'- balance = null, 'cause maximum possible value of the withdraw is balance.
     * @param money {@code BigDecimal} value that substract from the current balance.
     */
    public void withdrawFromBalance(BigDecimal money) {
        if (balance.compareTo(money) >= 0) {
            balance = balance.subtract(money);
        } else {
            balance = null;
        }
    }

    /**
     * convert method returns balance in different currency that depends from rate.
     * @param rate {@double} coefficient of exchange rate.
     * @return new balance status.
     */
    private BigDecimal convert(double rate) {
        return balance = balance.multiply(BigDecimal.valueOf(rate));
    }

    /**
     * displayDifferentCurrency method display Card balance after convert method
     * @param card {@code Card}
     * @param rate {@double} coefficient of exchange rate.
     */
    public void displayDifferentCurrency(Card card, double rate) {
        System.out.print("Card balance in different currency = " + card.convert(rate));
    }
}