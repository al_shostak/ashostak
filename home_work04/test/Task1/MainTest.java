package by.htp.home_work.hw4.Task1.Task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MainTest {
    Main main;

    @Before
    public void initMain(){
        main = new Main();
    }
    @After
    public void clearMain(){
        main = null;
    }

    @Test
    public void testMainBubbleSort() throws Exception {
        Main.main(new String[]{"a"});
        int[] actual = main.massive;
        int[] expected = new int[]{1, 2, 3, 3, 5, 6, 21, 34, 35, 56, 74};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testMainSelectionSort() throws Exception {
        Main.main(new String[]{"b"});
        int[] actual = main.massive;
        int[] expected = new int[]{1, 2, 3, 3, 5, 6, 21, 34, 35, 56, 74};
        assertArrayEquals(expected, actual);
    }

    @Test(expected = Exception.class)
    public void testMainWrongArg() throws Exception {
        Main.main(new String[]{"d"});
        int[] actual = main.massive;
        int[] expected = new int[]{1, 2, 3, 3, 5, 6, 21, 34, 35, 56, 74};
        assertArrayEquals(expected, actual);
    }
}

