package by.htp.home_work.hw4.Task1;

import by.htp.home_work.hw4.Task1.Card;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CreditCardTest {
    protected by.htp.home_work.hw4.Task1.Card card;
    protected String name;
    protected BigDecimal bd;
    BigDecimal cash;
    BigDecimal bigWithdraw;
    double index;

    @Before
    public void initCard() {
        name = new String ("Soyer");
        bd = new BigDecimal("222.111");
        bigWithdraw = new BigDecimal("333.333");
        card = new CreditCard(name, bd);
        cash = new BigDecimal("20.233");
        index = 0.1;
    }

    @After
    public void clearCard() {
        bd = null;
        card = null;
        cash = null;
        index = 0;
    }

    @Test
    public void testSetBalance() {
        Assert.assertEquals(card.getBalance(),bd);
    }

    @Test
    public void testGetBalance() {
        Assert.assertEquals(card.getBalance(),bd);
    }

    @Test
    public void testSetName() {
        Assert.assertEquals(card.getName(),"Soyer");
    }

    @Test
    public void testGetName() {
        Assert.assertEquals(card.getName(), name);
    }

    @Test
    public void testWithdrawFromBalance() {
        card.withdrawFromBalance(cash);
        Assert.assertEquals(card.getBalance(),BigDecimal.valueOf(201.878));
    }

    @Test
    public void testDeposit() {
        card.deposit(cash);
        Assert.assertEquals(card.getBalance(),BigDecimal.valueOf(242.344));
    }
    @Test
    public void testIfWithdrawBiggerThenBalance(){
        card.withdrawFromBalance(bigWithdraw);
        Assert.assertEquals(card.getBalance(),BigDecimal.valueOf(-111.222));
    }
}