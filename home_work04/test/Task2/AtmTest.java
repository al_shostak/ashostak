package by.htp.home_work.hw4.Task1;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class AtmTest {
    protected by.htp.home_work.hw4.Task1.Card card;
    protected String name;
    protected BigDecimal bd;
    CreditCard cCard;
    DebitCard dCard;
    BigDecimal bigWithdraw;
    Atm atm;


    @Before
    public void initCard() {
        name = new String("John");
        bd = new BigDecimal("222.111");
        bigWithdraw = new BigDecimal("333.333");
        dCard = new DebitCard(name, bd);
        cCard = new CreditCard(name, bd);
        atm = new Atm();

    }

    @After
    public void clearCard() {
        bd = null;
        dCard = null;
        cCard = null;
        atm = null;
    }

    @Test
    public void testWithdrawFromDebitCard() {
        atm.withdrawFromBalance(dCard, bigWithdraw);
        Assert.assertEquals(dCard.getBalance(), null);
    }

    @Test
    public void testWithdrawFromCreditCard() {
        atm.withdrawFromBalance(cCard, bigWithdraw);
        Assert.assertEquals(cCard.getBalance(), BigDecimal.valueOf(-111.222));
    }

    @Test
    public void testDeposit() {
        atm.deposit(dCard, bigWithdraw);
        Assert.assertEquals(dCard.getBalance(), BigDecimal.valueOf(555.444));
    }
}