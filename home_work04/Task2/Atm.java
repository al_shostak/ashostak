package by.htp.home_work.hw4.Task1;

import java.math.BigDecimal;
/**
 * Using transferred bank cards performs operations on withdrawing money,
 * replenishing the balance
 */
public class Atm {
    /**
     * Subtract from the balance the value of money.
     * @param card card for operations
     * @param money value will be subtract from the current balance.
     */
    public void withdrawFromBalance(Card card, BigDecimal money){
         card.withdrawFromBalance(money);
    }
    /**
     * deposit method adds money to user's balance.
     * @param money (@code BigDecimal)
     */
    public void deposit(Card card, BigDecimal money){
        card.deposit(money);
    }
}
