package by.htp.home_work.hw4.Task1;

import by.htp.home_work.hw4.Task1.Card;

import java.math.BigDecimal;

/**
 * The {@code DebitCard} class provides operations for deposit,
 * withdraw and viewing balance. Balance stored in {@code BigDecimal}.
 * Debit card does not allow negative balance value
 */
public class DebitCard extends Card {

    /**
     * Public constructor - creat a new card with specific values.
     *
     * @param name    {@code String} card owner's name.
     * @param balance {@code BigDecemal} the amount of money on the card.
     */
    public DebitCard(String name, BigDecimal balance) {
        super(name, balance);
    }

    /**
     * setBalance method allows to set values of balance.
     *
     * @param balance (@BigDecimal) this value will be assigned as a class field value.
     */
    @Override
    public void setBalance(BigDecimal balance) {
        super.setBalance(balance);
    }

    /**
     * @return the value of balance field.
     */
    @Override
    public BigDecimal getBalance() {
        return super.getBalance();
    }

    /**
     * setName method allows to set value of user's name field.
     *
     * @param name this value will be assigned as a class field value/
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     * @return the value of name field.
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     * withdrawFromBalance method substract money from balance. If balance status less than the value of
     * "withdraw money'- balance = null, 'cause maximum possible value of the withdraw is balance.
     *
     * @param money (@code BigDecimal) value that substract from the current balance.
     */
    @Override
    public void withdrawFromBalance(BigDecimal money) {
        if (balance.compareTo(money) >= 0) {
            balance = balance.subtract(money);
        } else {
            balance = null;
        }
    }

    /**
     * deposit method adds money to balance.
     *
     * @param money (@code BigDecimal)
     */
    @Override
    public void deposit(BigDecimal money) {
        super.deposit(money);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
