package by.htp.home_work.hw4.Task1;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * The {@code Card} class provides methods for deposit,
 * withdraw, convert, viewing balance status. Balance stored in {@code BigDecimal}.
 */
public class Card {

    protected String name;
    protected BigDecimal balance;

    /**
     * Public constructor - creat a new card with specific values.
     *
     * @param name    {@code String} card owner's name.
     * @param balance {@code BigDecemal} the amount of money on the card.
     */
    public Card(String name, BigDecimal balance) {
        this.name = name;
        this.balance = balance;
    }

    /**
     * public constructor - creation of a new object with specific value.
     *
     * @param name{@code String} card owner's name.
     */
    public Card(String name) {
        this.name = name;
    }

    /**
     * setBalance method allows to set values of balance.
     *
     * @param balance (@BigDecimal) this value will be assigned as a class field value.
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return the value of balance field.
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * setName method allows to set value of user's name field.
     *
     * @param name this value will be assigned as a class field value/
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value of name field.
     */
    public String getName() {
        return name;
    }

    /**
     * deposit method adds money to balance.
     *
     * @param money (@code BigDecimal)
     */
    public void deposit(BigDecimal money) {
        balance = balance.add(money);
    }

    /**
     * withdrawFromBalance method substract money from balance. If balance status less than the value of
     * "withdraw money'- balance = null, 'cause maximum possible value of the withdraw is balance.
     *
     * @param money (@code BigDecimal) value that substract from the current balance.
     */
    public void withdrawFromBalance(BigDecimal money) {
        balance = balance.subtract(money);
    }

    /**
     * convert method returns balance in different currency that depends from rate.
     *
     * @param rate (@double) coefficient of exchange rate.
     * @return new balance status.
     */
    private BigDecimal convert(double rate) {
        return balance = balance.multiply(BigDecimal.valueOf(rate));
    }

    /**
     * displayDifferentCurrency method display Card balance after convert method
     *
     * @param card (@code Card)
     * @param rate (@double) coefficient of exchange rate.
     */
    public void displayDifferentCurrency(Card card, double rate) {
        System.out.print("Card balance in different currency = " + card.convert(rate));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return name.equals(card.name) &&
                balance.equals(card.balance);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode())
                + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}