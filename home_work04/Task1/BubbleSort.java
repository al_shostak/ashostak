package by.htp.home_work.hw4.Task1.Task1;

/**
 * Implements Sorter interface
 */
public class BubbleSort implements Sorter {

    /**
     * Sorts the passed array using Bubble sort method
     * @param array {@code int} array of integers that will be sort
     */
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int var = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = var;
                }
            }
        }
    }
}

