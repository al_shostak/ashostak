package by.htp.home_work.hw4.Task1.Task1;

/**
 * Implements Sorter interface
 */
public class SelectionSort implements Sorter {
    /**
     * Sorts the passed array using Selection sort method
     * @param array {@code int} array of integers that will be sort
     */
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            int var = array[i];
            array[i] = array[min];
            array[min] = var;
        }
    }
}

