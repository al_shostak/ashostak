package by.htp.home_work.hw4.Task1.Task1;

/**
 * Sorter provides a way to sort an array of integers in ascending order
 */
public interface Sorter {

    public void sort(int [] array);
}
