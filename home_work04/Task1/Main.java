package by.htp.home_work.hw4.Task1.Task1;

/**
 * Calls the sorting method depending on the chosen strategy.
 * Creates and populates an array
 */
import java.util.Random;

public class Main {
    static final String STRATEGY_BUBBLE = "a";
    static final String STRATEGY_SELECTION = "b";
    static int [] massive = new int [] {2, 3, 56, 34, 21, 5, 6, 74, 35, 3, 1};

    public static void main(String[] args) throws Exception {
        Sorter sorter;
        String sortStrategy = args[0];
        if(sortStrategy.equals(STRATEGY_BUBBLE)){
            sorter = new BubbleSort();
        } else if (sortStrategy.equals(STRATEGY_SELECTION)){
            sorter = new SelectionSort();
        } else {
            throw new Exception("Parameter entered incorrectly!");
        }
        SortingContext context = new SortingContext(sorter);
        context.execute(massive);
    }
}
