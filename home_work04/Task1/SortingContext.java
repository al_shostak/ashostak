package by.htp.home_work.hw4.Task1.Task1;

/**
 * Delegates array sorting to a passed object Sorter
 */
public class SortingContext {
    private Sorter sorter;

    /**
     * Create object SortingContext based on the transferred object Sorter
     *
     * @param sorter object Sorter to be used
     */
    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * @return the value of Sorter field
     */
    public Sorter getSorter() {
        return sorter;
    }

    /**
     * Assigns a value to a field sorter
     *
     * @param sorter this value will be assigned as a class field value
     */
    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * Causes a certain kind of sorting
     *
     * @param array {@code int} array that will be sort
     */
    public void execute(int[] array) {
        sorter.sort(array);
    }
}
