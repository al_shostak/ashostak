package by.htp.home_work.hw6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
        String s = reader.readLine();
            System.out.println();
            SortText myText = new SortText(s);
            String newText = myText.deletePunctuation(s);
            ArrayList<String> text = myText.split(newText);
            myText.groupWords(text);

        }catch (IOException e){
            System.out.println("Something wrong");
        }
    }
}
