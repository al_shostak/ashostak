package by.htp.home_work.hw6;

import java.util.*;

public class SortText {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SortText(String text) {
        this.text = text;
    }

    /**
     * Method deletePunctuation removes punctuation marks.
     * @param text given String
     * @return lowercase text without punctuation.
     */
    public String deletePunctuation(String text) {
        String newText = text.replaceAll("\\W", " ");
        return newText.toLowerCase();
    }

    /**
     * Split method  breaks a given string around matches of the given regular expression
     * @param text given String
     * @return ArrayList<String> which contains the split Strings.
     */
    public ArrayList<String> split(String text) {
        String[] textArray = text.split(" ");
        ArrayList<String> array = new ArrayList<String>();
        for (int i = 0; i < textArray.length; i++) {
            if (!textArray[i].equals("")) {
                array.add(textArray[i]);
            }
        }
        return array;
    }

    /**
     * Method groups words by first letter in alphabetical order
     * @param myText is ArrayList that contains String
     */
    public void groupWords(ArrayList<String> myText) {

        Collections.sort(myText, String.CASE_INSENSITIVE_ORDER);
        Map<String, Integer> word = new TreeMap<String, Integer>();

        for (int i = 0; i < myText.size(); i++) {
            String string = myText.get(i);
            word.put(string, (word.get(string) == null ? 1 : (word.get(string) + 1)));
        }
        for (Map.Entry<String, Integer> entry : word.entrySet()) {
           String key = entry.getKey();
           Integer value = entry.getValue();
           System.out.printf(entry.getKey().toUpperCase().charAt(0) + "  " + "%s : %s\n", key, value);
        }
    }
}



