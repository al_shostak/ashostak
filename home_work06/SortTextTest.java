package by.htp.home_work.hw6;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.*;

public class SortTextTest {
    private SortText sortText;
    private String text;

    @Before
    public void initSortText() {
        text = "Once upon a time, a Wolf.";
        sortText = new SortText(text);
    }

    @After
    public void clearSortText() {
        text = null;
        sortText = null;
    }

    @Test
    public void testDeletePunctuation() {
        Assert.assertEquals(sortText.deletePunctuation(text), "once upon a time  a wolf ");
    }

    @Test
    public void testSplit() {
        ArrayList<String> expected = new ArrayList<String>();
        expected.add("once");
        expected.add("upon");
        expected.add("a");
        expected.add("time");
        expected.add("a");
        expected.add("wolf");
        String actual = "once upon a time  a wolf ";
        Assert.assertEquals(expected, sortText.split(actual));
    }

    @Test
    public void testGroupWords() {
      //  Map<String, Integer> expected = new TreeMap<String, Integer>();
      //  expected.put("a", 2);
      //  expected.put("once", 1);
      //  expected.put("time", 1);
      //  expected.put("upon", 1);
       // expected.put("wolf", 1);
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        String actual1 = "once upon a time  a wolf ";
        ArrayList<String>  actual = sortText.split(actual1);
        sortText.groupWords(actual);
        OutputStream str1 = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str1));
        System.out.println( "A  a : 2\nO  once : 1\nT  time : 1\nU  upon : 1\nW  wolf : 1");
        Assert.assertEquals( str.toString(),str1.toString());
        }
    }
