package by.htp.home_work;

import java.util.Random;

public class MedianExtra {

    private static boolean left = true;

    private MedianExtra() {
    }

    /**
     * Method median find "middle number" in an unsorted list of numbers.
     *
     * @param massive array contains a certain number of values of the type int.
     * @return value of "middle number" of type float.
     */
    public static float median(int[] massive) {
        float result;
        int[] array = massive;
        int index = massive.length / 2;
        if (massive.length % 2 == 0) {
            result = 0.5f * (findNumber(massive, index) + findNumber(massive, index - 1));
        } else {
            result = findNumber(massive, index);
        }
        return result;
    }

    /**
     * findNumber method looks up a value by index as if the array is sorted.
     *
     * @param array contains a certain number of values of the type int.
     * @param index the item number we are looking for.
     * @return value of the element number "index".
     */
    private static float findNumber(int[] array, int index) {
        while (array.length > 1) {
            if (isEqual(array) == true) {
                return array[0];
            }
            int size = array.length;
            array = quickSelect(array, index);
            if (left == false) {
                int minus = size - array.length;
                index -= minus;
                left = true;
            }
        }
        return (float) (array[0]);
    }

    /**
     * isEqual method check whether the array consists of all the same elements
     *
     * @param args contains a certain number of values of the type int.
     * @return true/false
     */
    private static boolean isEqual(int[] args) {
        int first = args[0];
        for (int i = 1; i < args.length; i++) {
            if (first == args[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * quickSelect method find "subarray" with the element we need.
     *
     * @param arr   array with elements type int
     * @param index number of element.
     * @return {@code int} array with the element we are looking for.
     */
    private static int[] quickSelect(int[] arr, int index) {
        Random rand = new Random();
        int r = rand.nextInt(arr.length);
        int fundamental = arr[r];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= fundamental) {
                count++;
            }
        }
        int[] less = new int[count];
        int lessCounter = 0;
        int[] big = new int[arr.length - count];
        int bigCounter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= fundamental) {
                less[lessCounter] = arr[i];
                lessCounter++;
            } else {
                big[bigCounter] = arr[i];
                bigCounter++;
            }
        }
        if (lessCounter >= (index + 1)) {
            return less;
        } else {
            left = false;
            return big;
        }
    }

    /**
     * Method median find "middle number" in an unsorted list of numbers.
     *
     * @param args array contains a certain number of values of the type double.
     * @return value of "middle number" of type double.
     */
    public static double median(double[] args) {
        double result;
        double[] array = args;
        int index = args.length / 2;
        if (args.length % 2 == 0) {
            result = 0.5f * (findNumber(args, index) + findNumber(args, index - 1));
        } else {
            result = findNumber(args, index);
        }
        return result;

    }

    /**
     * findNumber method looks up a value by index as if the array is sorted.
     *
     * @param array contains a certain number of values of the type double.
     * @param index the item number we are looking for.
     * @return value of the element number "index".
     */
    private static double findNumber(double[] array, int index) {
        while (array.length > 1) {
            if (isEqual(array) == true) {
                return array[0];
            }
            int size = array.length;
            array = quickSelect(array, index);
            if (left == false) {
                int minus = size - array.length;
                index = index - minus;
                left = true;
            }
        }
        return (array[0]);
    }

    /**
     * isEqual method check whether the array consists of all the same elements
     *
     * @param args contains a certain number of values of the type double.
     * @return boolean (true/false)
     */
    private static boolean isEqual(double[] args) {
        double first = args[0];
        for (int i = 1; i < args.length; i++) {
            if (first == args[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * quickSelect method find "subarray" with the element we need.
     *
     * @param arr   array with elements type double.
     * @param index number of the element.
     * @return {@code double} array with the element we are looking for.
     */
    private static double[] quickSelect(double[] arr, int index) {
        Random rand = new Random();
        int r = rand.nextInt(arr.length);
        double fundamental = arr[r];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= fundamental) {
                count++;
            }
        }
        double[] less = new double[count];
        int lessCounter = 0;
        double[] big = new double[arr.length - count];
        int bigCounter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= fundamental) {
                less[lessCounter] = arr[i];
                lessCounter++;
            } else {
                big[bigCounter] = arr[i];
                bigCounter++;
            }
        }
        if (lessCounter >= (index + 1)) {
            return less;
        } else {
            left = false;
            return big;
        }
    }
}
