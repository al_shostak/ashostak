package by.htp.home_work.hw2;

public class Task2 {

    /**
     * Print results of chosen alrorithm by loop type and user's data.
     *
     * @params args {@code Array} that contains user's data.
     */
    public static void main(String[] args) {
        final int ALGORITHM = Integer.parseInt(args[0]);
        final int LOOP_TYPE = Integer.parseInt(args[1]);
        final int N = Integer.parseInt(args[2]);

        if (ALGORITHM == 1) {
            if (LOOP_TYPE == 1) {
                printFibonacci(fibonacciWhile(N), N);
            } else if (LOOP_TYPE == 2) {
                printFibonacci(fibonacciDoWhile(N), N);
            } else if (LOOP_TYPE == 3) {
                printFibonacci(fibonacciFor(N), N);
            } else {
                System.out.print("Incorrectly entered number!");
            }
        } else if (ALGORITHM == 2) {
            if (LOOP_TYPE == 1) {
                printFactorial(factorialWhile(N), N);
            } else if (LOOP_TYPE == 2) {
                printFactorial(factorialDoWhile(N), N);
            } else if (LOOP_TYPE == 3) {
                printFactorial(factorialFor(N), N);
            } else {
                System.out.print("Incorrectly entered number!");
            }
        } else {
            System.out.print("Incorrectly entered number!");
        }
    }

    /**
     * Returns the {@code array} containing the specified number of fibonacci numbers
     *
     * @param N {@code int} number of returned Fibonacci numbers
     * @return N Fibonacci numbers
     */
    public static int[] fibonacciWhile(final int N) {
        int iCurrent = 0;
        int iPast = 0;
        int sum = 1;
        int[] array = new int[N];
        int arrayIndex = 0;
        while (arrayIndex < N) {
            array[arrayIndex] = iCurrent;
            arrayIndex++;
            sum += iPast;
            iPast = iCurrent;
            iCurrent = sum;
        }
        return array;
    }

    /**
     * Returns the {@code array} containing the specified number of fibonacci numbers
     *
     * @param N {@code int} number of returned Fibonacci numbers
     * @return N Fibonacci numbers
     */
    public static int[] fibonacciDoWhile(final int N) {
        int iCurrent = 0;
        int iPast = 0;
        int sum = 1;
        int arrayIndex = 0;
        int[] array = new int[N];
        do {
            array[arrayIndex] = iCurrent;
            arrayIndex++;
            sum += iPast;
            iPast = iCurrent;
            iCurrent = sum;
        } while (arrayIndex < N);
        return array;
    }

    /**
     * Returns the {@code array} containing the specified number of fibonacci numbers
     *
     * @param N {@code int} number of returned Fibonacci numbers
     * @return N Fibonacci numbers
     */
    public static int[] fibonacciFor(final int N) {
        int sum = 1;
        int iPast = 0;
        int arrayIndex = 0;
        int[] array = new int[N];
        for (int iCurrent = 0; arrayIndex < N; iCurrent = sum) {
            sum += iPast;
            iPast = iCurrent;
            array[arrayIndex] = iCurrent;
            arrayIndex++;
        }
        return array;
    }

    /**
     * Returns the {@code array} containing the factorial value of given number
     *
     * @param N {@code int} number for factorial calculation
     * @return the result of given number
     */
    public static int factorialWhile(final int N) {
        int result = 1;
        int i = 1;
        while (i <= N) {
            result *= i;
            i++;
        }
        return result;
    }

    /**
     * Returns the {@code array} containing the factorial value of given number
     *
     * @param N {@code int} number for factorial calculation
     * @return the result of given number
     */
    public static int factorialDoWhile(final int N) {
        int result = 1;
        int i = 1;
        do {
            result *= i;
            i++;
        } while (i <= N);
        return result;
    }

    /**
     * Returns the {@code array} containing the factorial value of given number
     *
     * @param N {@code int} number for factorial calculation
     * @return factorial of given number
     */
    public static int factorialFor(final int N) {
        int result = 1;
        for (int i = 1; i <= N; i++) {
            result *= i;
        }
        return result;
    }

    public static void printFibonacci(int[] massive, final int N) {
        for (int p : massive) {
            System.out.print(p + ",");
        }
        System.out.println(" - " + N + " first Fibonacci numbers  ");
    }

    public static void printFactorial(int factorial, final int N) {
        System.out.print("Factorial of a number " + N + " = " + factorial);
    }
}



