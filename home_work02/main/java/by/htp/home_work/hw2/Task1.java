package by.htp.home_work.hw2;

public class Task1 {

    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int p = Integer.parseInt(args[1]);
        double m = Double.parseDouble(args[2]);
        double m2 = Double.parseDouble(args[3]);

        if ((m + m2) != 0 && p != 0) {
            double result = calculate(a, p, m, m2);
            System.out.println("Value of the function: " + result);
        } else {
            System.out.print("Divider is zero!!");
        }
    }

    /**
     * Returns the value of the function.
     * @params - users  data.
     */
    public static double calculate(int a, int p, double m, double m2) {
        return (4 * (Math.pow(Math.PI, 2))) * (Math.pow(a, 3)) / (Math.pow(p, 2) * (m + m2));
    }
}
