package by.htp.home_work.hw2;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Task1Test {

    @Test
    public void testCalculate() {
        double expected = Task1.calculate(1, 2, 3.4, 5.6);
        Assert.assertEquals(expected, 1.095, 0.01);
    }

    @Test
    public void testMainDividerIsZero(){
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        Task1.main(new String [] {"1","0","3.4","3.5"} );
        Assert.assertEquals("Divider is zero!", str.toString());
    }

}