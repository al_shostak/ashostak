package by.htp.home_work.hw2;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Task2Test {
    @Test
    public void testFibonacciWhile() {
        int n = 3;
        int[] expected = Task2.fibonacciWhile(n);
        int[] actual = {0,1,1};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFibonacciDoWhile() {
        int n = 3;
        int[] expected = Task2.fibonacciDoWhile(n);
        int[] actual = {0, 1, 1};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFibonacciFor() {
        int n = 3;
        int[] expected = Task2.fibonacciFor(n);
        int[] actual = {0, 1, 1};
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFactorialWhile() {
        int n = 4;
        int expected = Task2.factorialWhile(n);
        int actual = 24;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialDoWhile() {
        int n = 4;
        int expected = Task2.factorialDoWhile(n);
        int actual = 24;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFactorialFor() {
        int n = 4;
        int expected = Task2.factorialFor(n);
        int actual = 24;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMainIsWrongData(){
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        Task2.main(new String [] {"1", "6", "2"});
        Assert.assertEquals("Incorrectly entered number!", str.toString() );
    }

}