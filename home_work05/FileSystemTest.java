package by.htp.home_work.hw5;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class FileSystemTest {
    private String path;
    private String path1;

    @Before
    public void initFileSystemTest() {
        path = "root/folder/file.exe";
        path1 = "root/folder1/file1.exe";
    }

    @After
    public void clearFileSystemTest() {
        path = null;
        path1 = null;
    }

    @Test
    public void testMain() {

        Component actual = new Builder().build(path.split("/"));
        Component expected = new Folder("root", new ArrayList<Component>());
        Component folder = new Folder("folder", new ArrayList<Component>());
        folder.add(new File("file", "exe"));
        expected.add(folder);
        Assert.assertEquals(expected, actual);

    }

    @Test
    public void testMerge() throws Exception {

        Component actual1 = new Builder().build(path.split("/"));
        Component actual2 = new Builder().build(path1.split("/"));
        Component expected = new Folder("root", new ArrayList<Component>());
        Component folder = new Folder("folder", new ArrayList<Component>());
        Component folder1 = new Folder("folder1", new ArrayList<Component>());
        folder.add(new File("file", "exe"));
        folder1.add(new File("file1", "exe"));
        expected.add(folder);
        expected.add(folder1);
        Component actual = Builder.merge(actual1, actual2);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testParser() {
        boolean expected = true;
        boolean actual = Parser.validate(path);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testParserWrong() {
        boolean expected = false;
        boolean actual = Parser.validate("root/file.exe.txt");
        Assert.assertEquals(expected, actual);
    }
}