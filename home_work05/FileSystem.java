package by.htp.home_work.hw5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FileSystem {
    /**
     * Method accepts an input string representing the path to the file /
     * directory, added to the root element "root", adds new directories /
     * files to an existing path(merge them).
     * Calls a method to display directory structures and
     * method to exit the program.
     * @throws Exception-  if the path to the file / directory is entered incorrectly.
     */
    public static void main(String[] args) throws Exception {
        Boolean isParental = true;
        Component component = new Folder("root", new ArrayList<Component>());
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String str = reader.readLine();
                if (str.equals("exit")) {
                    break;
                } else if (str.equals("print")) {
                    component.print(0);
                    continue;
                }

                if ((Parser.validate(str) == true) && (isParental == true)) {
                    component = new Builder().build(str.split("/"));
                    isParental = false;
                } else if ((Parser.validate(str) == true) && (isParental == false)) {
                    Component compParent = component;
                    Component compChild = new Builder().build(str.split("/"));
                    component = Builder.merge(compParent, compChild);
                } else {
                    throw new Exception("Wrong");
                }
            }
        }
    }
}
