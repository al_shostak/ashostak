package by.htp.home_work.hw5;

import java.util.Objects;

public class File implements Component{
    private String name;
    private String extension;

    public File(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }


    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method displays the directory structure.
     * @param i  the number of indent for better visualization of the directory structure.
     */
    public void print(int i) {
        for(int j = 0; j < i; j++) {
            System.out.print(" ");
        }
        System.out.println(name + "." + extension);
    }


    @Override
    public void add(Component component) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return name.equals(file.name) &&
                extension.equals(file.extension);
    }

   @Override
   public int hashCode() {
       final int prime = 31;
       int result = 1;
       result = prime * result + ((name == null) ? 0 : name.hashCode())
               + ((name == null) ? 0 : name.hashCode());
       return result;
   }

    @Override
    public String toString() {
        return "File{" +
                "name='" + name + '\'' +
                ", extension='" + extension + '\'' +
                '}';
    }
}
