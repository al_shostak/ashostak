package by.htp.home_work.hw5;

import java.util.List;

public class Folder implements Component {
    private String name;
    private List<Component> components;

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public Folder(String name, List<Component> components) {
        this.name = name;
        this.components = components;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method adds other new files and directories.
     * @param component is a file or directory that we want to add.
     */
    public void add(Component component) {
        boolean isNewComponent = true;
        for (Component comp : components) {
            if ((comp.getName().equals(component.getName()))) {
                isNewComponent = false;
                break;
            }
        }
        if (isNewComponent == true & !(component.getName().equals("root"))) {
            components.add(component);
        }
    }

    /**
     * Method displays the directory structure.
     * @param i the number of indent for better visualization of the directory structure.
     */
    public void print(int i) {
        for (int j = 0; j < i; j++) {
            System.out.print(" ");
        }
        System.out.println(name);
        i = i + 4;
        for (Component comp : components) {
            comp.print(i);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return name.equals(folder.name) &&
                components.equals(folder.components);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode())
                + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Folder{" +
                "name='" + name + '\'' +
                ", components=" + components +
                '}';
    }
}
