package by.htp.home_work.hw5;

import java.util.ArrayList;

public class Builder {
    static boolean isFind = false;

    /**
     * Method build- calls method to create a file or a folder.
     * @param elements array of input data
     * @return created structure
     */
    public Component build(String[] elements) {
        Component component;
        String name = elements[0];
        if (isFolder(elements[0]) == true) {
            if (elements.length == 1) {
                component = buildFolder(elements[0]);
            } else {
                String[] newElements = new String[elements.length - 1];
                for (int i = 0; i < elements.length - 1; i++) {
                    newElements[i] = elements[i + 1];
                }
                component = buildFolder(name, newElements);
            }
        } else {
            component = buildFile(elements);
        }
        return component;
    }

    /**
     * Method create a folder.
     * @param name of the folder.
     * @param newElements new directories or files.
     * @return created folder.
     */
    private Component buildFolder(String name, String[] newElements) {
        Folder folder = new Folder(name, new ArrayList<Component>());
        folder.add(build(newElements));
        return folder;
    }

    /**
     * Method create a folder.
     * @param name of the folder.
     * @return created folder.
     */
    private Component buildFolder(String name) {
        Folder folder = new Folder(name, new ArrayList<Component>());
        return folder;
    }

    /**
     * Method create a file.
     * @param str array with input data for building a file.
     * @return created file.
     */
    private Component buildFile(String[] str) {
        String[] args = str[0].split("\\.");
        File file = new File(args[0], args[1]);
        return file;
    }

    /**
     * method checks if a component is a folder.
     * @param name folder's name
     * @return boolean true/false
     */
    private boolean isFolder(String name) {
        if (name.contains(".")) {
            return false;
        }
        return true;
    }

    /**
     * Method adds new directories to an existing path.
     * @param comp1 old folder/file
     * @param comp2 new folder/file
     * @return new structure
     * @throws Exception - if the path to the file / directory is entered incorrectly.
     */
    public static Component merge(Component comp1, Component comp2) throws Exception {
        compareChilds(comp1, comp2);
        isFind = false;
        return comp1;
    }

    /**
     * Method compares two components, if they are equal, it skips and takes
     * the next couple of components, if different then adds this component.
     * @param component file/folder
     * @param comp2 file/folder
     */
    private static void compareChilds(Component component, Component comp2){

        if (comp2.getClass().equals(File.class)) {
            component.add(comp2);
        }
        for (Component child : ((Folder) comp2).getComponents()) {

            for (Component biggerChild : ((Folder) component).getComponents()) {

                if (biggerChild.getName().equals(child.getName())) {
                    compareChilds(biggerChild, child);
                } else {
                    continue;
                }
            }
            if (isFind == false) {
                ((Folder) component).getComponents().add(child);
                isFind = true;
                break;
            }
        }
    }
}
