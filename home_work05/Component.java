package by.htp.home_work.hw5;

public interface Component {
    /**
     * Method add other files and directories, in random order, an arbitrary number.
     * @param component a file or a directory.
     */
    public void add(Component component);

    /**
     * Method displays the resulting directory structure.
     * @param i the number of indent for better visualization of the directory structure.
     */
    public void print(int i);

    /**
     * Method returns the name of the desired directory structure.
     * @return name.
     */
    public String getName();
}
