package by.htp.home_work.hw5;

import java.util.regex.Pattern;

public class Parser {
    /**
     * Method splits the string into elements and checks for
     * compliance with the standard writing path
     * @param str the path to the file or directory
     * @return boolean true/false
     */
    public static boolean validate(String str) {
        String [] elements = str.split("/");
        boolean isCorrect = true;
        Pattern componentPattern = Pattern.compile("\\w+\\.?\\w+");
        for (String current : elements) {
            if (!current.matches(componentPattern.toString())) {
                isCorrect = false;
                break;
            }
        }
        return isCorrect;
    }
}
